# -*- coding: utf-8 -*-
#main author: Leszek Czerwosz

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
x_len = 100 # Number of points to display
y_rangeY = [0, 1000] 
y_rangeV = [-500, 500]
y_rangeA = [-500, 500]
xmouse = 0
ymouse = 0
alfa = 2
scatfig = plt.figure(figsize =(5,5))
scatfig.set_size_inches(4, 6.5) #pionowy slupek
plt.grid(which='major', axis='both')
#https://stackoverflow.com/questions/7449585/how-do-you-set-theabsolute-position-of-figure-windows-with-matplotli

mngr = plt.get_current_fig_manager()
# to put it into the upper right
mngr.window.setGeometry(1100,30,600, 600)
ax = plt.axes(xlim=(0, 1000), ylim=(0, 1000),aspect='equal')
patchCOG = plt.Circle((500, 500), 10.75, color='blue', fill=True)
ax.add_patch(patchCOG)
patchCOP = plt.Circle((500, 500), 10.75, color='orange', 
fill=True)
ax.add_patch(patchCOP)
plt.grid(which='major', axis='both')
# Create figure for plotting
#fig = plt.figure(figsize=(10,11))
fig = plt.figure()
axY = fig.add_subplot(3, 1, 1)
axV = fig.add_subplot(3, 1, 2)
axA = fig.add_subplot(3, 1, 3)
mngr = plt.get_current_fig_manager()
# to put it into the upper right
mngr.window.setGeometry(10,30,1050, 1000)
#plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, 
right=0.95, hspace=0.7,
# wspace=0.99)
axY.set_title('blue=Mouse Y position model of COG orange=COP')
axY.grid(which='major', axis='both')
axV.set_title('mouse Y velocity = 1st derivative')
axV.grid(which='major', axis='both')
axA.set_title('mouse Y acceleration = 2st derivative')
axA.grid(which='major', axis='both')
xs = list(range(0, x_len))
COGx = [0] * x_len
COGy = [0] * x_len
COGvx = [0] * x_len
COGvy = [0] * x_len
COGax = [0] * x_len
COGay = [0] * x_len

COPx = [0] * x_len
COPy = [0] * x_len
axY.set_ylim(y_rangeY)
axY.text(0, 1.2, "alfa={:10.4f}".format(alfa), fontsize=12, 
transform=axY.transAxes)
axV.set_ylim(y_rangeV)
axA.set_ylim(y_rangeA)
lineG, = axY.plot(xs, COGy)
lineP, = axY.plot(xs, COPy)
lineV, = axV.plot(xs, COGvy)
lineA, = axA.plot(xs, COGay)
pause = False
def onmove(event):
 global xmouse,ymouse
 xmouse = event.x
 ymouse = event.y
def onClick(event):
 global pause
 pause ^= True
 
# This function is called periodically from FuncAnimation
def animate(i1,COGx, COGy, COGvx, COGvy, COGax, COGay, COPx, 
COPy):
 global xmouse, ymouse, alfa
 
 if not pause:
 #ymouse =400* np.sin(2 * np.pi * (0.01 * i1)) + 400
 #COG
 COGx.append(xmouse)
 xx = xmouse
 COGx = COGx[-x_len:]
 COGy.append(ymouse)
 yy = ymouse
 
 COGy = COGy[-x_len:]
 
 #last = (COGy[x_len-2] + COGy[x_len-3] + COGy[x_len-4])/3
 last = COGx[x_len-2]
 dx = COGy[x_len-1]-last
 COGvx.append(dx)
 COGvx = COGvx[-x_len:]
 last = COGy[x_len-2]
 dy = COGy[x_len-1]-last
 COGvy.append(dy)
 COGvy = COGvy[-x_len:]
 #last = (COGv[x_len-2] + COGv[x_len-3] + COGv[x_len-4])/3
 last = COGvx[x_len-2]
 dvx = COGvx[x_len-1]-last
 COGax.append(dvx)
 COGax = COGax[-x_len:]
 last = COGvy[x_len-2]
 dvy = COGvy[x_len-1]-last
 COGay.append(dvy)
 COGay = COGay[-x_len:]
 #ys2 = np.gradient(ys1)
 #ys3 = np.gradient(ys2)
 
 
 #COP
 x = xmouse - alfa*(dvx)
 COPx.append(x)
 COPx = COPx[-x_len:]
 y = ymouse - alfa*(dvy)
 COPy.append(y)
 COPy = COPy[-x_len:]
 
 
 lineG.set_ydata(COGy)
 lineP.set_ydata(COPy)
 lineV.set_ydata(COGvy)
 lineA.set_ydata(COGay)

patchCOG.center = (xx, yy)
 patchCOP.center = (x, y)
 scatfig.canvas.draw()
 scatfig.canvas.flush_events()
 
 return lineG,lineV,lineA,lineP, scatfig,
cid = fig.canvas.mpl_connect('motion_notify_event',onmove)
#cid = scatfig.canvas.mpl_connect('motion_notify_event',onmove)
#scatfig.canvas.mpl_connect('button_press_event', onClick)
fig.canvas.mpl_connect('button_press_event', onClick)
# Set up plot to call animate() function periodically
ani1 = animation.FuncAnimation(fig,
 animate,
 fargs=(COGx, COGy, COGvx, COGvy, COGax, COGay, COPx, COPy,),
 interval= 10,
 blit=False)
 # init_func=init)
plt.show()

 
 

